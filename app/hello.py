from flask import Flask, Response, request
import requests
import hashlib
import redis

app = Flask(__name__)

cache = redis.StrictRedis(host='redis', port=6379, db=0)
salt = "UNIQUE_SALT"  # pass this in thru env?


@app.route('/', methods=['GET', 'POST'])
def get_identicon():

    ip = request.environ['REMOTE_ADDR']
    if request.method == 'POST':
        ip = request.form['name']

    salted_ip = salt + ip
    ip_hash = hashlib.sha256(salted_ip.encode()).hexdigest()

    ret = '<html><head><title>Hello</title></head>'
    #ret += '<body>Hello {} <img src="/monster.png"/></body></html>'.format(ip)
    form = '<form method="POST"><input type="text" name="name" value="{}"><input type="submit" value="submit"></form>'.format(ip)
    ret += '<body>Hello {0} <img src="/monster/{1}"/></body></html>'.format(form, ip_hash)
    return ret


@app.route('/monster/<name>')
def get_identicon2(name):

    #check if have cached version
    image = cache.get(name)
    if image is None:
        print ("Cache miss", flush=True)
        #Probably move address and salt to config, possibly size
        r = requests.get('http://dnmonster:8080/monster/' + name + '?size=80')
        image = r.content
        cache.set(name, image)

    return Response(image, mimetype='image/png')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
